import json
import requests
from .keys import PEXELS_API_KEY

def get_photo(city, state):
    # Use the Pexels API
    header = {"Authorization": PEXELS_API_KEY}
    params = {
        #query is required stated by the pexels api documentation
        "query": city + " " + state,
        "per_page": 1
    }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, header=header)
    content = response.json()

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}




def get_weather_data(city, state):
    # Use the Open Weather API
    pass
